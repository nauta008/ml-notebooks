FROM jupyter/datascience-notebook:ubuntu-22.04

RUN wget -O isimip3b.zip https://zenodo.org/api/records/10256871/files/isimip3b.zip/content 
RUN unzip isimip3b.zip && rm isimip3b.zip

COPY --chown=${NB_UID}:${NB_GID} ./exercises ./work