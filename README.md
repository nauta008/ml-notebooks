# ML notebooks

[[_TOC_]]

## Installation

**Requirements**
- docker
- git (optional)

Install the [Docker Engine](https://docs.docker.com/engine/install/).

Choose between:
- [Build Docker Image](#build-docker-image) (prefered)
- [Using the container registry](#using-the-container-registry)

### Build Docker Image

Clone the repository.

```bash
git clone https://git.wur.nl/nauta008/ml-notebooks
```

Run `cd ml-notebooks` to navigate to the root of the project. Build the image and run the container.

```bash
docker compose up
```

Open a browser and navigate to <localhost:8888>. Work on your exercises and when you are finished run:

```bash
docker compose down
```

### Using the container registry
Download the docker image for an amd64 architecture.

```bash
docker pull docker-registry.wur.nl/nauta008/ml-notebooks:latest
```

Run the following command to start Jupyter Lab. We mounted a volume into the `work` folder to store your progress. Keep in mind that your progress is only stored in this `work` folder.

```bash
docker run -p 8888:8888 --name ml-notebooks -v ml-notebooks:/home/jovyan/work docker-registry.wur.nl/nauta008/ml-notebooks start.sh jupyter lab --NotebookApp.token=''
```
Open a browser and navigate to <localhost:8888>

## Data
<https://zenodo.org/records/10256871>

